package ejercicios00;

public class EjemploNotacion {
	//las constantes se declaran aqui.
	static final int ALGUNA_CONSTANTE=43;
	static final double PEPE_ALGO = 8573.686432;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int numero1 = 5;
		//La altura importa a la hora de poner el syso, la variable debe estar inicializada (=)
		int numero2 = 3;
		int numeroGordo = 95;
		System.out.println(numero1);
		System.out.println(numeroGordo);
		String cadena = "hey";
		String cadenaMasLarga = "Pues algo \n mas largo";
		System.out.println(cadena);
		System.out.println(cadenaMasLarga);
		double numerico = 2.84;
		double numericoGordo = 4.5;
		System.out.println(numerico);
		System.out.println(numericoGordo);
		final double LOL=937642392;
		System.out.println(LOL);
		char letra = 'y';
		boolean algunaVariable;
		algunaVariable=true;
		System.out.println(letra);
		System.out.println(algunaVariable);
		System.out.println(ALGUNA_CONSTANTE);
		System.out.println(PEPE_ALGO);
		float random = 6.8F;
		System.out.println(random);
		int olgao =7;
		int yep = 4;
		int suma = olgao + yep;
		System.out.println("suma de olgao y yep es "+ suma );

	}

}
